(function($) {
  
  "use strict";

    // function callback($id ,defTop) {
    //   $(window).on('scroll', function() {
    //     console.log('SCROLL', defTop)
    //     $($id).css("padding-top", defTop)
    //   })
    // }
    // $('#service_about .service_about_menu li a').on('click', function() { 
    //   console.log('ANCHORE')
    //   var $id = $(this).attr('href'),
    //     defTop= $($id).css('padding-top');
    //   console.log($id, defTop);
    //   $($id).css("padding-top", "+=68")

    //   callback($id, defTop);
    //   // setTimeout(function() {
    //   //   $(window).on('scroll', function() {
    //   //   console.log('SCROLL', defTop)
    //   //   $($id).css("padding-top", defTop)
    //   // }) }
    //   //  , 1000)
    
    // });

  // Sticky Nav
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 200) {
            $('.scrolling-navbar').addClass('top-nav-collapse');
        } else {
            $('.scrolling-navbar').removeClass('top-nav-collapse');
        }
    });

    /* 
   One Page Navigation & wow js
   ========================================================================== */
    //Initiat WOW JS
    new WOW().init();

    // one page navigation 
    $('.main-navigation').onePageNav({
            currentClass: 'active'
    }); 

    $(window).on('load', function() {
       
        $('body').scrollspy({
            target: '.navbar-collapse',
            offset: 195
        });

        $(window).on('scroll', function() {
            if ($(window).scrollTop() > 200) {
                $('.fixed-top').addClass('menu-bg');
            } else {
                $('.fixed-top').removeClass('menu-bg');
            }
        });

    });

    // Slick Nav 
    $('.mobile-menu').slicknav({
      prependTo: '.navbar-header',
      parentTag: 'span',
      allowParentLinks: false,
      duplicate: false,
      label: '',
    });


/* 
   CounterUp
   ========================================================================== */
    $('.counter').counterUp({
      time: 1000
    });

/* 
   MixitUp
   ========================================================================== */
  $('#portfolio').mixItUp();

/* 
   Touch Owl Carousel
   ========================================================================== */
    var owl = $(".touch-slider");
    owl.owlCarousel({
      navigation: false,
      pagination: true,
      slideSpeed: 1000,
      stopOnHover: true,
      autoPlay: true,
      items: 2,
      itemsDesktop : [1199,2],
      itemsDesktopSmall: [1024, 2],
      itemsTablet: [600, 1],
      itemsMobile: [479, 1]
    });

    $('.touch-slider').find('.owl-prev').html('<i class="fa fa-chevron-left"></i>');
    $('.touch-slider').find('.owl-next').html('<i class="fa fa-chevron-right"></i>');

/* 
   Sticky Nav
   ========================================================================== */
    $(window).on('scroll', function() {
        if ($(window).scrollTop() > 200) {
            $('.header-top-area').addClass('menu-bg');
        } else {
            $('.header-top-area').removeClass('menu-bg');
        }
    });

/* 
   VIDEO POP-UP
   ========================================================================== */
    $('.video-popup').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
    });


  /* 
   SMOOTH SCROLL
   ========================================================================== */
    var scrollAnimationTime = 1200,
        scrollAnimation = 'easeInOutExpo';

    $('a.scrollto').on('bind', 'click.smoothscroll', function (event) {
        event.preventDefault();
        var target = this.hash;
        
        $('html, body').stop().animate({
            'scrollTop': $(target).offset().top
        }, scrollAnimationTime, scrollAnimation, function () {
            window.location.hash = target;
        });
    });

/* 
   Back Top Link
   ========================================================================== */
    var offset = 200;
    var duration = 500;
    $(window).scroll(function() {
      if ($(this).scrollTop() > offset) {
        $('.back-to-top').fadeIn(400);
      } else {
        $('.back-to-top').fadeOut(400);
      }
    });

    $('.back-to-top').on('click',function(event) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: 0
      }, 600);
      return false;
    })

/* Nivo Lightbox
  ========================================================*/   
   $('.lightbox').nivoLightbox({
    effect: 'fadeScale',
    keyboardNav: true,
  });


/* stellar js
  ========================================================*/
  $.stellar({
    horizontalScrolling: true,
    verticalOffset: 40,
    responsive: true
  });

/* 
   Page Loader
   ========================================================================== */
  $('#loader').fadeOut();

  // var offset = $('.target').offset();
  // console.log('offset', offset)
  // if (offset) {
  //   var scrollto = offset.top - 68; // minus fixed header height
  //   $('html, body').animate({scrollTop:scrollto}, 0);
  //   // $('[data-toggle="tooltip"]').tooltip();
  // }

  /* */
  $('#industry_travel .points_list li').on('click', function(){
    let $this = $(this),
        $list = $this.parent(),
        $panelContent = $('#industry_travel .panel-content'),
        dataPanelPane = $this.data('pane');

    $list.find('.selected').removeClass('selected');
    $this.addClass('selected');

    $panelContent.find('.selected').removeClass('selected');
    $(dataPanelPane).addClass('selected')
  });

    $('.projects_menu li a').on('click', function() {
    let $activate = $(this).attr('href'),
        $projContent = $('.projects_content')

    $projContent.find('.tab-pane').removeClass('active')
    $projContent.find($activate).addClass('active')

  })

  $('.modal-contact').on('click', function(e){
    console.log('MODAL HERE')
    e.preventDefault();
    $("#modal_contact").modal();
  })

  $('.nav-anchor').on('click', function() {
    console.log('click')
     var hrf = $(this).attr('href');

     console.log(hrf)
     window.location.href = hrf;
   })

  $('#filter_category_form select').on('change', function() {
    $('#filter_category_form').submit();
  })

  $('#filter_category_form input').on('focusout', function() {
    $('#filter_category_form').submit();
  })

  $('.slicknav_nav li ul.slicknav_hidden').each(function() {
    // console.log('MOBILE', $(this).html())
    if (!$.trim($(this).html())) {
      $(this).parent().find('.slicknav_arrow').remove()
    }
  });

  // Slick Slider JS

  $('#experience').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    // asNavFor: '#exp-handler'
  });

  $('#exp-handler').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    // asNavFor: '#experience',
    centerMode: true,
    focusOnSelect: true,
    variableWidth: true,
    arrows: true,
    dots: false,
    prevArrow: $('.exp-slider-arl'),
    nextArrow: $('.exp-slider-arr')
  });

  $('.exp-content-images .exp-image-item').on('click', function() {
    let $this = $(this)
    $this.parent().find('.exp-image-item').removeClass('selected')
    $this.addClass('selected')
    $this.parent().prepend($this)
  }) 

}(jQuery));
